<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/')]
class VisitorController extends AbstractController
{
    #[Route('', name: 'homepage')]
    public function homePage(){
        return $this->render("visitor/homepage.html.twig");
    }
}
