<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

#[Route('admin/restaurants')]
class AdminCrudRestaurant extends AbstractController {

    #[Route('', name: 'admin_list_restaurant')]
    public function listRestaurants(){
        return $this->render('admin/restaurants/list.html.twig');
    }

    #[Route('/new', name: 'admin_add_restaurant')]
    public function addRestaurant(){
        return $this->render('admin/restaurants/add.html.twig');
    }
}