<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminCuisineTypeController extends AbstractController
{
    #[Route('/admin/cuisine-type', name: 'admin_type_cuisine')]
    public function index(): Response
    {
        return $this->render('admin/cuisine-type/list.html.twig', [
        ]);
    }
}
